#pragma once

#include "Modules/ModuleManager.h"
#include "IInputDeviceModule.h"

class IDisplayClusterInputModule : public IInputDeviceModule
{
public:
	static inline IDisplayClusterInputModule& Get()
	{
		return FModuleManager::LoadModuleChecked<IDisplayClusterInputModule>("DisplayClusterInput");
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("DisplayClusterInput");
	}
};
