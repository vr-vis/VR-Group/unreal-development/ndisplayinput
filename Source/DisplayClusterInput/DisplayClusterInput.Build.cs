using UnrealBuildTool;

public class DisplayClusterInput : ModuleRules
{
  public DisplayClusterInput(ReadOnlyTargetRules Target) : base(Target)
  {
    PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

    PublicIncludePaths.AddRange(
      new string[]
      {

      }
      );


    PrivateIncludePaths.AddRange(
      new string[]
      {

      }
      );


    PublicDependencyModuleNames.AddRange(
      new string[]
      {
        "Core",
        "CoreUObject",
        "DisplayCluster",
        "Engine",
        "InputCore",
        "InputDevice"
      }
      );


    PrivateDependencyModuleNames.AddRange(
      new string[]
      {

      }
      );


    DynamicallyLoadedModuleNames.AddRange(
      new string[]
      {

      }
      );
  }
}
