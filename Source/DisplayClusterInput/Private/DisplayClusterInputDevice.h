#pragma once

#include "IInputDevice.h"

class FDisplayClusterInputDevice : public IInputDevice
{
public:
  FDisplayClusterInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler);
  virtual ~FDisplayClusterInputDevice();

  static  void PreInit             ();

	virtual bool Exec                (UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar)                   override;
	virtual void SendControllerEvents()                                                                       override;
	virtual void SetChannelValue     (int32 ControllerId, FForceFeedbackChannelType ChannelType, float Value) override;
	virtual void SetChannelValues    (int32 ControllerId, const FForceFeedbackValues& Values)                 override;
	virtual void SetMessageHandler   (const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)  override;
	virtual void Tick                (float DeltaTime)                                                        override;

private:
  TSharedPtr<FGenericApplicationMessageHandler> MessageHandler;
  TArray<const FKey*>                           AxisIndices   ;
  TArray<const FKey*>                           ButtonIndices ;
};