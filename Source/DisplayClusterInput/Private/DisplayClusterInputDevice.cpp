#include "DisplayClusterInputDevice.h"

#include "CoreMinimal.h"
#include "Features/IModularFeatures.h"
#include "Input/IDisplayClusterInputManager.h"
#include "GameFramework/PlayerInput.h"
#include "GameFramework/InputSettings.h"
#include "IDisplayCluster.h"

#include "DisplayClusterKeys.h"

#define LOCTEXT_NAMESPACE "DisplayClusterInput"

FDisplayClusterInputDevice::FDisplayClusterInputDevice(
	const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler) : MessageHandler(InMessageHandler)
{
	AxisIndices = TArray<const FKey*>
	{
		&FDisplayClusterKeys::Axis0,
		&FDisplayClusterKeys::Axis1
	};
	//Reordered to match the 4.22 behavior
	ButtonIndices = TArray<const FKey*>
	{
		&FDisplayClusterKeys::Button0,
		&FDisplayClusterKeys::Button4,
		&FDisplayClusterKeys::Button3,
		&FDisplayClusterKeys::Button2,
		&FDisplayClusterKeys::Button1,
		&FDisplayClusterKeys::Button5,
		&FDisplayClusterKeys::Button6,
		&FDisplayClusterKeys::Button7,
		&FDisplayClusterKeys::Button8,
		&FDisplayClusterKeys::Button9
	};
}

FDisplayClusterInputDevice::~FDisplayClusterInputDevice()
{
}

void FDisplayClusterInputDevice::PreInit()
{
	EKeys::AddMenuCategoryDisplayInfo("DisplayCluster", LOCTEXT("DisplayClusterSubCategory", "Display Cluster"),
	                                  TEXT("GraphEditor.PadEvent_16x"));

	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button0, LOCTEXT("nDisplayButton0", "nDisplay Button 0"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button1, LOCTEXT("nDisplayButton1", "nDisplay Button 1"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button2, LOCTEXT("nDisplayButton2", "nDisplay Button 2"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button3, LOCTEXT("nDisplayButton3", "nDisplay Button 3"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button4, LOCTEXT("nDisplayButton4", "nDisplay Button 4"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button5, LOCTEXT("nDisplayButton5", "nDisplay Button 5"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button6, LOCTEXT("nDisplayButton6", "nDisplay Button 6"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button7, LOCTEXT("nDisplayButton7", "nDisplay Button 7"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button8, LOCTEXT("nDisplayButton8", "nDisplay Button 8"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Button9, LOCTEXT("nDisplayButton9", "nDisplay Button 9"),
	                          FKeyDetails::GamepadKey, "DisplayCluster"));

	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Axis0, LOCTEXT("nDisplayAxis0", "nDisplay Axis 0"),
	                          FKeyDetails::FloatAxis, "DisplayCluster"));
	EKeys::AddKey(FKeyDetails(FDisplayClusterKeys::Axis1, LOCTEXT("nDisplayAxis1", "nDisplay Axis 1"),
	                          FKeyDetails::FloatAxis, "DisplayCluster"));

	FInputAxisConfigEntry AxisX, AxisY;
	AxisX.AxisKeyName = FDisplayClusterKeys::Axis0.GetFName();
	AxisX.AxisProperties = FInputAxisProperties();
	AxisY.AxisKeyName = FDisplayClusterKeys::Axis1.GetFName();
	AxisY.AxisProperties = FInputAxisProperties();
	UInputSettings::GetInputSettings()->AxisConfig.Add(AxisX);
	UInputSettings::GetInputSettings()->AxisConfig.Add(AxisY);
}

bool FDisplayClusterInputDevice::Exec(UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar)
{
	return false;
}

void FDisplayClusterInputDevice::SendControllerEvents()
{
	if (!IDisplayCluster::IsAvailable()) return;

	auto InputManager = IDisplayCluster::Get().GetInputMgr();

	TArray<FString> AxisDeviceIds;
	InputManager->GetAxisDeviceIds(AxisDeviceIds);
	for (auto i = 0; i < AxisDeviceIds.Num(); ++i)
	{
		for (auto j = 0; j < AxisIndices.Num(); ++j)
		{
			float Value = 0.0f;
			InputManager->GetAxis(AxisDeviceIds[i], j, Value);
			MessageHandler->OnControllerAnalog(AxisIndices[j]->GetFName(), i, Value);
		}
	}

	TArray<FString> ButtonDeviceIds;
	InputManager->GetButtonDeviceIds(ButtonDeviceIds);
	for (auto i = 0; i < ButtonDeviceIds.Num(); ++i)
	{
		for (auto j = 0; j < ButtonIndices.Num(); ++j)
		{
			bool Pressed = false, Released = false;
			InputManager->WasButtonPressed(ButtonDeviceIds[i], j, Pressed);
			InputManager->WasButtonReleased(ButtonDeviceIds[i], j, Released);
			if (Pressed) MessageHandler->OnControllerButtonPressed(ButtonIndices[j]->GetFName(), i, false);
			if (Released) MessageHandler->OnControllerButtonReleased(ButtonIndices[j]->GetFName(), i, false);
		}
	}
}

void FDisplayClusterInputDevice::SetChannelValue(int32 ControllerId, FForceFeedbackChannelType ChannelType, float Value)
{
	// Intentionally empty (no haptic support).
}

void FDisplayClusterInputDevice::SetChannelValues(int32 ControllerId, const FForceFeedbackValues& Values)
{
	// Intentionally empty (no haptic support).
}

void FDisplayClusterInputDevice::SetMessageHandler(
	const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
	MessageHandler = InMessageHandler;
}

void FDisplayClusterInputDevice::Tick(float DeltaTime)
{
	// Intentionally empty (everything is handled in SendControllerEvents above).
}

#undef LOCTEXT_NAMESPACE
