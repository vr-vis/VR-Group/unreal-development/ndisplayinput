#include "DisplayClusterInputModule.h"

#include "IDisplayCluster.h"

#include "DisplayClusterInputDevice.h"

#define LOCTEXT_NAMESPACE "DisplayClusterInput"

void FDisplayClusterInputModule::StartupModule()
{
  IInputDeviceModule::StartupModule();
  FDisplayClusterInputDevice::PreInit();
}

TSharedPtr<class IInputDevice> FDisplayClusterInputModule::CreateInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
  return TSharedPtr<FDisplayClusterInputDevice>(new FDisplayClusterInputDevice(InMessageHandler));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FDisplayClusterInputModule, DisplayClusterInput)
