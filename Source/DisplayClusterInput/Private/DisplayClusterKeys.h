#pragma once

#include "InputCoreTypes.h"

struct FDisplayClusterKeys
{
  static const FKey Button0;
  static const FKey Button1;
  static const FKey Button2;
  static const FKey Button3;
  static const FKey Button4;
  static const FKey Button5;
  static const FKey Button6;
  static const FKey Button7;
  static const FKey Button8;
  static const FKey Button9;

  static const FKey Axis0  ;
  static const FKey Axis1  ;
};