#include "DisplayClusterKeys.h"

const FKey FDisplayClusterKeys::Button0("nDisplayButton0");
const FKey FDisplayClusterKeys::Button1("nDisplayButton1");
const FKey FDisplayClusterKeys::Button2("nDisplayButton2");
const FKey FDisplayClusterKeys::Button3("nDisplayButton3");
const FKey FDisplayClusterKeys::Button4("nDisplayButton4");
const FKey FDisplayClusterKeys::Button5("nDisplayButton5");
const FKey FDisplayClusterKeys::Button6("nDisplayButton6");
const FKey FDisplayClusterKeys::Button7("nDisplayButton7");
const FKey FDisplayClusterKeys::Button8("nDisplayButton8");
const FKey FDisplayClusterKeys::Button9("nDisplayButton9");

const FKey FDisplayClusterKeys::Axis0  ("nDisplayAxis0"  );
const FKey FDisplayClusterKeys::Axis1  ("nDisplayAxis1"  );
