#pragma once

#include "SharedPointer.h"
#include "IInputDevice.h"

#include "IDisplayClusterInputModule.h"

class FDisplayClusterInputModule : public IDisplayClusterInputModule
{
  virtual void                           StartupModule    ()                                                                      override;
  virtual TSharedPtr<class IInputDevice> CreateInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler) override;
};